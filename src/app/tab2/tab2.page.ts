import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  LocationService,
  MyLocation,
  CameraPosition,
  MarkerOptions,
  Marker,
  Environment
} from '@ionic-native/google-maps/ngx';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {

  map: GoogleMap;

  italyOptions: GoogleMapOptions = {
    camera: {
      target: {
        lat: 41.902457,
        lng: 12.535792
      },
      zoom: 6,
      tilt: 30
    }
  };

  constructor(private platform: Platform) { }

  async ngOnInit() {
    await this.platform.ready();
    await this.loadMap();
  }

  loadMap() {
    // This code is necessary for browser
    Environment.setEnv({
      'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyDOJK1ulDLoQa_gY7GSUbWY_MZ3fkkrv5Q',
      'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyDOJK1ulDLoQa_gY7GSUbWY_MZ3fkkrv5Q'
    });

    // Init the maps with Italy map
    this.map = GoogleMaps.create('map_canvas', this.italyOptions);

    // Imposta la posizione del gps
    LocationService.getMyLocation().then((myLocation: MyLocation) => {
      console.log(myLocation);
      this.map.setCameraTarget(myLocation.latLng);
      this.map.setCameraZoom(18);

    });
  }

}
